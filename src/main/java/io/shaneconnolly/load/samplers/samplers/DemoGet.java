package io.shaneconnolly.load.samplers.samplers;

import io.shaneconnolly.load.constants.GeneralConstants;
import io.shaneconnolly.load.samplers.helpers.ResponseAssertions;
import io.shaneconnolly.load.samplers.helpers.HttpHelper;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;

import java.io.Serializable;

/**
 * @author connollys
 *         25/02/15.
 */
public class DemoGet extends AbstractJavaSamplerClient implements Serializable {
    private static final long serialVersionUID = 234234;
    private JMeterVariables threadVars;
    private SampleResult result;
    private static String samplerName = "DEMO";

    private void setUp() {
        threadVars = JMeterContextService.getContext().getVariables();

    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
        setUp();
        result = HttpHelper.executeHttpGet(GeneralConstants.TEST_ENV + "/api", samplerName);
        verifyResponse(result.getResponseDataAsString());
        return result;
    }

    private void verifyResponse(String json) {
        ResponseAssertions.verifyAvailability(json, result);
    }

    @Override
    public void setupTest(JavaSamplerContext context){
    }

    @Override
    public void teardownTest(JavaSamplerContext context) {

    }

    public JMeterContext getThreadContext() {
        return JMeterContextService.getContext();
    }


    public static String getSamplerName(){
        return samplerName;
    }

    // new methods

}
