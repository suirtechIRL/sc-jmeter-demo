package io.shaneconnolly.load.samplers.helpers;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

import javax.naming.Context;
import java.io.BufferedInputStream;
import java.security.KeyStore;

/**
 * @author connollys
 *         24/08/15.
 */

public class SampleClient extends DefaultHttpClient {

    final Context context;

    public SampleClient(Context context) {
        this.context = context;
    }

    @Override protected ClientConnectionManager createClientConnectionManager() {
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(
                new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", newSslSocketFactory(), 443));
        return new SingleClientConnManager(getParams(), registry);
    }

    private SSLSocketFactory newSslSocketFactory() {
        KeyStore trustStore = null;
        BufferedInputStream stream = null;
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            stream = new BufferedInputStream(DefaultHttpClient.class.getResourceAsStream("/demo.keystore"));
            trustStore.load(stream, "50000".toCharArray());
            return new SSLSocketFactory(trustStore);
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }
}