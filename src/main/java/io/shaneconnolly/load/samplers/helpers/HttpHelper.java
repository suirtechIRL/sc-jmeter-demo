package io.shaneconnolly.load.samplers.helpers;

import io.shaneconnolly.load.constants.GeneralConstants;
import io.shaneconnolly.load.sharedhelpers.LoadException;
import io.shaneconnolly.load.sharedhelpers.LogHelper;
import io.shaneconnolly.load.sharedhelpers.ResultHelper;
import io.shaneconnolly.load.constants.ProjectConstants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @author connollys
 *         25/02/15.
 */
public class HttpHelper {

    public static SampleResult executeHttpPost(String url, String resultLabel, String requestBody) {
        SampleResult result = new SampleResult();
        result.sampleStart(); // start stopwatch
        DefaultHttpClient httpClient = HttpHelper.getHttpClient();
        HttpPost httpPost;
        String responseBody;
        try {
            httpPost = HttpHelper.getNoSessionHeaders(url);
            StringEntity flights = new StringEntity(requestBody);
            LogHelper.logInfo("TID: " + JMeterContextService.getContext().getThread().getThreadNum() + " | Executing request " + httpPost.getRequestLine());
            httpPost.setEntity(flights);
            responseBody = httpClient.execute(httpPost, HttpHelper.getResponseHandlerForSessionHeader(result));


            result = ResultHelper.setSuccessfulResults(result, requestBody);
            result.setResponseData(responseBody.getBytes());
        } catch (Exception e) {
            result = ResultHelper.setErrorResults(result, e, requestBody);
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
            result.sampleEnd();
            LogHelper.logInfo(String.format("%s | HTTP CODE: %s", resultLabel, result.getResponseCode()));
            result.setSampleLabel(resultLabel);
        }
        return result;
    }

    public static SampleResult executeHttpGet(String url, String resultLabel) {
        SampleResult result = new SampleResult();
        result.sampleStart(); // start stopwatch
        DefaultHttpClient httpClient = HttpHelper.getHttpClient();
        JMeterVariables threadVars = JMeterContextService.getContext().getVariables();
        try {

            HttpGet httpget = HttpHelper.getHttpGet(url, threadVars.get("TOKEN"));
            LogHelper.logInfo("TID: " + JMeterContextService.getContext().getThread().getThreadNum() + " | Executing request " + httpget.getRequestLine());
            LogHelper.logInfo("Header-> " + httpget.getHeaders("X-Session-Token")[0].getValue());


            String responseBody = httpClient.execute(httpget, HttpHelper.getResponseHandler(result));
            result.setResponseData(responseBody.getBytes());
            result.setDataType(SampleResult.TEXT);
            result = ResultHelper.setSuccessfulResults(result, url);
            result.sampleEnd(); // stop stopwatch
            result.setSuccessful(true);
            result.setResponseMessage("Successfully performed action");
            result.setResponseCodeOK(); // 200 code
            result.setSampleLabel(resultLabel);


        } catch (Exception e) {
            result = ResultHelper.setErrorResults(result, e, url);
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
            LogHelper.logInfo(String.format("%s | HTTP CODE: %s", resultLabel, result.getResponseCode()));
        }
        return result;
    }


    public static SampleResult executeHttpPut(String url, String resultLabel, String requestBody) {
        SampleResult result = new SampleResult();
        JMeterVariables threadVars = JMeterContextService.getContext().getVariables();
        result.sampleStart(); // start stopwatch
        result.setSampleLabel(resultLabel);
        DefaultHttpClient httpClient = HttpHelper.getHttpClient();
        String responseBody;
        try {
            HttpPut httpPut = getHttpPut(url, threadVars.get("TOKEN"));
            StringEntity flights = new StringEntity(requestBody);
            httpPut.setEntity(flights);
//            LogHelper.logInfo("TID: " + JMeterContextService.getContext().getThread().getThreadNum() + " | Executing PUT request " + httpPut.getRequestLine());
            responseBody = httpClient.execute(httpPut, HttpHelper.getResponseHandler(result));
            result = ResultHelper.setSuccessfulResults(result, requestBody);
            result.setResponseData(responseBody.getBytes());
        } catch (Exception e) {
            result = ResultHelper.setErrorResults(result, e, requestBody);
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
            LogHelper.logInfo(String.format("%s | HTTP CODE: %s", resultLabel, result.getResponseCode()));
            result.sampleEnd();
        }
        return result;
    }


    public static HttpPost getNoSessionHeaders(String urlString) {
        HttpPost httpPost = new HttpPost(urlString);
        httpPost.addHeader("Content-Type", "application/json");
//        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("Connection", "keep-alive");
        httpPost.addHeader("Accept", "*/*");
        return httpPost;
    }

    public static HttpPost getPostHeaders(String urlString, String token) {
        HttpPost httpPost = new HttpPost(urlString);
        httpPost.addHeader("Content-Type", "application/json");
//        httpPost.addHeader("Accept-Encoding", "gzip, deflate");
        httpPost.addHeader("Connection", "keep-alive");
        httpPost.addHeader("Accept", "*/*");
        httpPost.addHeader("Token", token);
        return httpPost;
    }

    public static HttpPut getHttpPut(String urlString, String token) {
        HttpPut httpPut = new HttpPut(urlString);
        httpPut.addHeader("Content-Type", "application/json");
//        httpPut.addHeader("Accept-Encoding", "gzip, deflate");
        httpPut.addHeader("Connection", "keep-alive");
        httpPut.addHeader("Accept", "*/*");
        httpPut.addHeader("Token", token);
        return httpPut;

    }

    public static HttpGet getHttpGet(String urlString, String token) {
        HttpGet httpGet = new HttpGet(urlString);
        httpGet.addHeader("Content-Type", "application/json");
        httpGet.addHeader("X-Session-Token", token);
        return httpGet;

    }

    public static HttpDelete getHttpDelete(String urlString, String token) {
        HttpDelete httpDelete = new HttpDelete(urlString);
        httpDelete.addHeader("Content-Type", "application/json");
        httpDelete.addHeader("X-Session-Token", token);
        return httpDelete;

    }

    public static ResponseHandler<String> getResponseHandler(final SampleResult result) {
        return new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws IOException {
                int status = response.getStatusLine().getStatusCode();
                result.setResponseCode(String.valueOf(status));
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    entity.getContentEncoding();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    ResultHelper.setErrorResultsBadResponse(result, "Response BAD CODE");
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };
    }

    public static ResponseHandler<String> getResponseHandlerForSessionHeader(final SampleResult result) {
        return new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws IOException {
                int status = response.getStatusLine().getStatusCode();
                result.setResponseCode(String.valueOf(status));
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    ResultHelper.setErrorResultsBadResponse(result, "Response BAD CODE");
                    LoadException.throwParseException(String.format("SeatGet RESPONSE Unexpected response status: %s", status));
                }
                return null;
            }
        };
    }

    public static TrustManager[] getTrustManager() {
        return new TrustManager[]{
                new X509TrustManager() {

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }

                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }
                }
        };

    }

    /**
     * Get A HttpClient With SSL Cert
     *
     * @return
     */
    public static DefaultHttpClient getHttpClient() {

        try {
            return httpClientTrustingAllSSLCerts();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static DefaultHttpClient httpClientTrustingAllSSLCerts() throws NoSuchAlgorithmException, KeyManagementException {
        DefaultHttpClient httpclient = new DefaultHttpClient();

        if (GeneralConstants.USE_CHARLES) {
            HttpHost proxy = new HttpHost(ProjectConstants.PROXY_HOST, ProjectConstants.PROXY_PORT, "http");
            httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        }

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, getTrustManager(), new java.security.SecureRandom());

        SSLSocketFactory socketFactory = new SSLSocketFactory(sc);
        Scheme sch = new Scheme("https", 443, socketFactory);
        httpclient.getConnectionManager().getSchemeRegistry().register(sch);
        return httpclient;
    }

    private static DefaultHttpClient httpClientTrustingAllSSLCerts2() throws NoSuchAlgorithmException, KeyManagementException {
        DefaultHttpClient httpclient = new DefaultHttpClient();

        KeyStore trustStore = null;

        BufferedInputStream stream = null;
        KeyManager[] km = null;
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        TrustManager[] tm = tmf.getTrustManagers();
        try {
            tmf.init(trustStore);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }


        try {
            KeyStore keystore = KeyStore.getInstance("jks");
            ClassLoader cl = DefaultHttpClient.class.getClassLoader();
            URL url = cl.getResource("/charlesLoad.keystore");
            char[] pwd = "737800".toCharArray();
            keystore.load(url.openStream(), pwd);
            KeyManagerFactory kmfactory = KeyManagerFactory.getInstance(
                    KeyManagerFactory.getDefaultAlgorithm());
            try {
                kmfactory.init(keystore, pwd);
            } catch (UnrecoverableKeyException e) {
                e.printStackTrace();
            }
            km = kmfactory.getKeyManagers();
        } catch (NoSuchAlgorithmException | KeyStoreException | CertificateException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Trust own CA and all self-signed certs
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(km, tm, null);
        TrustStrategy trustStrategy = new TrustStrategy() {

            public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                for (X509Certificate cert : chain) {
                    System.err.println(cert);
                }
                return false;
            }

        };

        SSLSocketFactory sslsf = null;
        try {
            sslsf = new SSLSocketFactory("TLS", null, null, trustStore, null,
                    trustStrategy, new AllowAllHostnameVerifier());
        } catch (KeyStoreException | UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        Scheme https = new Scheme("https", 443, sslsf);
        httpclient.getConnectionManager().getSchemeRegistry().register(https);

        return httpclient;
    }
}