package io.shaneconnolly.load;

import io.shaneconnolly.load.constants.GeneralConstants;
import io.shaneconnolly.load.constants.SamplerConstants;
import io.shaneconnolly.load.sharedhelpers.SetUpHelper;
import io.shaneconnolly.load.samplers.samplers.DemoGet;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.backend.BackendListener;
import org.apache.jmeter.visualizers.backend.BackendListenerGui;
import org.apache.jorphan.collections.ListedHashTree;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author connollys
 *         25/02/15.
 */
public class RunLoad {
    private static int THREADS = GeneralConstants.THREADS;
    private static int RAMP_UP = GeneralConstants.RAMP_UP;
    private static int LOOPS = GeneralConstants.LOOPS;
    private static StandardJMeterEngine jmeter;

    public static void main(String[] argv) throws Exception {
        File jmeterHome = new File(GeneralConstants.JMETER_HOME);

        if (jmeterHome.exists()) {
            File jmeterProperties = new File(jmeterHome.getPath() + "/bin/jmeter.properties");

            if (jmeterProperties.exists()) {
                //JMeter Engine
                jmeter = new StandardJMeterEngine();

                //JMeter initialization (properties, log levels, locale, etc)
                JMeterUtils.setJMeterHome(jmeterHome.getPath());
                JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
                JMeterUtils.initLogging();                      // you can comment this line out to see extra log messages of i.e. DEBUG level
                JMeterUtils.initLocale();

                // JMeter Test Plan, basically JOrphan HashTree
                ListedHashTree testPlanTree = new ListedHashTree();


                // Loop Controller
                LoopController loopController = new LoopController();
                loopController.setName("Main Loop Controller");
                loopController.setLoops(LOOPS);
                loopController.setFirst(true);
                loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
                loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
                loopController.initialize();

                // Thread Group
                ThreadGroup threadGroup = new ThreadGroup();
                threadGroup.setName("THREAD_GROUP");
                threadGroup.setNumThreads(THREADS);
                threadGroup.setRampUp(RAMP_UP);
                threadGroup.setSamplerController(loopController);
                SetUpHelper.setObjectProperty(threadGroup);
                threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
                threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());
                threadGroup.setEnabled(true);

                // Test Plan
                TestPlan testPlan = new TestPlan("TestPlan");
                testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
                testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
                testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());
                testPlan.addThreadGroup(threadGroup);
                testPlan.setEnabled(true);

                ListedHashTree threadGroupHashTree = (ListedHashTree) testPlanTree.add(testPlan, threadGroup);
                threadGroupHashTree.add(SetUpHelper.createSampler(SamplerConstants.SAMPLER_1, DemoGet.class.getName()));
                threadGroupHashTree.add(SetUpHelper.createSampler(SamplerConstants.SAMPLER_2, DemoGet.class.getName()));
                Summariser summer = null;
                String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");

                if (summariserName.length() > 0) {
                    summer = new Summariser(summariserName);
                }

                BackendListener bl = new BackendListener();
                bl.setName("Grafana");
                bl.setProperty(TestElement.TEST_CLASS, BackendListener.class.getName());
                bl.setProperty(TestElement.GUI_CLASS, BackendListenerGui.class.getName());
                bl.setEnabled(true);
                Arguments args = new Arguments();

                String samplerList = "";
                for (String s : SamplerConstants.getListOfSamplers()) {
                    samplerList += s + ";";
                }

                args.addArgument("graphiteMetricSender", "io.shaneconnolly.load.backend.customegraphite.TextGraphiteMetricsSender");
                args.addArgument("graphiteHost", "127.0.0.1");
                args.addArgument("graphitePort", "2003");
                args.addArgument("rootMetricsPrefix", "sc.");
                args.addArgument("samplersList", "");
                args.addArgument("percentiles", "90;95;99");


                bl.setArguments(args);

                threadGroupHashTree.add(bl);

                // Save to jmx file
                SaveService.saveTree(testPlanTree, new FileOutputStream("./NAT-001.jmx"));

                // Run Test Plan
                jmeter.configure(testPlanTree);
//                jmeter.run();

                System.exit(0);

            }
        }

        System.err.println("jmeter.home property is not set or pointing to incorrect location");
        System.exit(1);


    }


}