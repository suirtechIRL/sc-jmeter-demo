package io.shaneconnolly.load.backend.customegraphite;


import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig;
import org.apache.jmeter.visualizers.backend.graphite.SocketConnectionInfos;
import org.apache.jmeter.visualizers.backend.graphite.SocketOutputStream;
import org.apache.jmeter.visualizers.backend.graphite.SocketOutputStreamPoolFactory;

/**
 * Base class for {@link GraphiteMetricsSender}
 * @since 2.13
 */
abstract class AbstractGraphiteMetricsSender implements GraphiteMetricsSender {

    /**
     * Create a new keyed pool of {@link SocketOutputStream}s using a
     * {@link SocketOutputStreamPoolFactory}. The keys for the pool are
     * {@link SocketConnectionInfos} instances.
     *
     * @return GenericKeyedObjectPool the newly generated pool
     */
    protected GenericKeyedObjectPool<SocketConnectionInfos, SocketOutputStream> createSocketOutputStreamPool() {
        GenericKeyedObjectPoolConfig config = new GenericKeyedObjectPoolConfig();
        config.setTestOnBorrow(true);
        config.setTestWhileIdle(true);
        config.setMaxTotalPerKey(-1);
        config.setMaxTotal(-1);
        config.setMaxIdlePerKey(-1);
        config.setMinEvictableIdleTimeMillis(TimeUnit.MINUTES.toMillis(3));
        config.setTimeBetweenEvictionRunsMillis(TimeUnit.MINUTES.toMillis(3));

        return new GenericKeyedObjectPool<>(
                new SocketOutputStreamPoolFactory(SOCKET_CONNECT_TIMEOUT_MS, SOCKET_TIMEOUT), config);
    }

    /**
     * Replaces Graphite reserved chars:
     * <ul>
     * <li>' ' by '-'</li>
     * <li>'\\' by '-'</li>
     * <li>'.' by '_'</li>
     * </ul>
     *
     * @param s
     *            text to be sanitized
     * @return the sanitized text
     */
    static String sanitizeString(String s) {
        // String#replace uses regexp
        return StringUtils.replaceChars(s, "\\ .", "--_");
    }
}
