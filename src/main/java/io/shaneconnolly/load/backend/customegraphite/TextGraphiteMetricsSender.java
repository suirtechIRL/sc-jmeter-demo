package io.shaneconnolly.load.backend.customegraphite;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.jmeter.visualizers.backend.graphite.SocketConnectionInfos;
import org.apache.jmeter.visualizers.backend.graphite.SocketOutputStream;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

/**
 * PlainText Graphite sender
 * @since 2.13
 */
class TextGraphiteMetricsSender extends AbstractGraphiteMetricsSender {
    private static final Logger LOG = LoggingManager.getLoggerForClass();

    private String prefix;

    private List<MetricTuple> metrics = new ArrayList<>();

    private GenericKeyedObjectPool<SocketConnectionInfos, SocketOutputStream> socketOutputStreamPool;

    private SocketConnectionInfos socketConnectionInfos;


    TextGraphiteMetricsSender() {
        super();
    }

    /**
     * @param graphiteHost Graphite Host
     * @param graphitePort Graphite Port
     * @param prefix Common Metrics prefix
     */
    @Override
    public void setup(String graphiteHost, int graphitePort, String prefix) {
        this.prefix = prefix;
        this.socketConnectionInfos = new SocketConnectionInfos(graphiteHost, graphitePort);
        this.socketOutputStreamPool = createSocketOutputStreamPool();

        if(LOG.isInfoEnabled()) {
            LOG.info("Created TextGraphiteMetricsSender with host:"+graphiteHost+", port:"+graphitePort+", prefix:"+prefix);
        }
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.visualizers.backend.graphite.GraphiteMetricsSender#addMetric(long, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void addMetric(long timestamp, String contextName, String metricName, String metricValue) {
        StringBuilder sb = new StringBuilder(50);
        sb
                .append(prefix)
                .append(contextName)
                .append(".")
                .append(metricName);
        metrics.add(new MetricTuple(sb.toString(), timestamp, metricValue));
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.visualizers.backend.graphite.GraphiteMetricsSender#writeAndSendMetrics()
     */
    @Override
    public void writeAndSendMetrics() {
        if (metrics.size()>0) {
            SocketOutputStream out = null;
            try {
                out = socketOutputStreamPool.borrowObject(socketConnectionInfos);
                // pw is not closed as it would close the underlying pooled out
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(out, CHARSET_NAME), false);
                for (MetricTuple metric: metrics) {
                    pw.printf("%s %s %d%n", metric.name, metric.value, Long.valueOf(metric.timestamp));
                }
                pw.flush();

                if(LOG.isDebugEnabled()) {
                    LOG.debug("Wrote "+ metrics.size() +" metrics");
                }
                socketOutputStreamPool.returnObject(socketConnectionInfos, out);
            } catch (Exception e) {
                if(out != null) {
                    try {
                        socketOutputStreamPool.invalidateObject(socketConnectionInfos, out);
                    } catch (Exception e1) {
                        LOG.warn("Exception invalidating socketOutputStream connected to graphite server '"+
                                socketConnectionInfos.getHost()+"':"+socketConnectionInfos.getPort(), e1);
                    }
                }
                LOG.error("Error writing to Graphite:"+e.getMessage());
            }
            metrics.clear();
        }
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.visualizers.backend.graphite.GraphiteMetricsSender#destroy()
     */
    @Override
    public void destroy() {
        socketOutputStreamPool.close();
    }

}
