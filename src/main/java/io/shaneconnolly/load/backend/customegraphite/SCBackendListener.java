package io.shaneconnolly.load.backend.customegraphite;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.visualizers.backend.graphite.GraphiteBackendListenerClient;
/**
 * Created by seany on 08/01/2017.
 */
public class SCBackendListener extends GraphiteBackendListenerClient {


    //+ Argument names
    // These are stored in the JMX file, so DO NOT CHANGE ANY VALUES
    private static final String GRAPHITE_METRICS_SENDER = "graphiteMetricsSender"; //$NON-NLS-1$
    private static final String GRAPHITE_HOST = "graphiteHost"; //$NON-NLS-1$
    private static final String GRAPHITE_PORT = "graphitePort"; //$NON-NLS-1$
    private static final String ROOT_METRICS_PREFIX = "rootMetricsPrefix"; //$NON-NLS-1$
    private static final String PERCENTILES = "percentiles"; //$NON-NLS-1$
    private static final String SAMPLERS_LIST = "samplersList"; //$NON-NLS-1$
    public static final String USE_REGEXP_FOR_SAMPLERS_LIST = "useRegexpForSamplersList"; //$NON-NLS-1$
    public static final String USE_REGEXP_FOR_SAMPLERS_LIST_DEFAULT = "false";
    private static final String SUMMARY_ONLY = "summaryOnly"; //$NON-NLS-1$

    private static final String DEFAULT_PERCENTILES = "90;95;99";
    private static final String DEFAULT_METRICS_PREFIX = "jmeter."; //$NON-NLS-1$
    private static final int DEFAULT_PLAINTEXT_PROTOCOL_PORT = 2003;


    @Override
    public Arguments getDefaultParameters() {
        Arguments arguments = new Arguments();
        arguments.addArgument(GRAPHITE_METRICS_SENDER, TextGraphiteMetricsSender.class.getName());
        arguments.addArgument(GRAPHITE_HOST, "");
        arguments.addArgument(GRAPHITE_PORT, Integer.toString(DEFAULT_PLAINTEXT_PROTOCOL_PORT));
        arguments.addArgument(ROOT_METRICS_PREFIX, DEFAULT_METRICS_PREFIX);
        arguments.addArgument(SUMMARY_ONLY, "false");
        arguments.addArgument(SAMPLERS_LIST, "");
        arguments.addArgument(USE_REGEXP_FOR_SAMPLERS_LIST, USE_REGEXP_FOR_SAMPLERS_LIST_DEFAULT);
        arguments.addArgument(PERCENTILES, DEFAULT_PERCENTILES);
        return arguments;
    }

}
