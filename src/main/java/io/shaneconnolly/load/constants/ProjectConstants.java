package io.shaneconnolly.load.constants;

public class ProjectConstants {

    //Email formats
    public static final String LOAD_EMAIL_FORMAT = "load%s@shaneconnolly.io";
    public static final String LOAD_UPDATED_EMAIL_FORMAT = "loadUpdated%s@shaneconnolly.io";
    public static final String TESTER_EMAIL_FORMAT = "tester%s@shaneconnolly.io";
    public static final String LOAD_TESTER_EMAIL_FORMAT = "loadTester_%s@shaneconnolly.io";

    //PROXY
    public static final String PROXY_HOST = "127.0.0.1";
    public static final int PROXY_PORT = 8888;

    public static String PROJECT_DATE_FORMAT = "yyyy-MM-dd"; //2015-02-15
    public static String MOBILE_DATE_FORMAT = "dd/MM/yyyy";
    public static String UTC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static String DATE_FORMAT = "yyyyMMddHHmmssSSS";

    public static final String DEFAULT_TEST2_ENDPOINT = "10.0.0.11";


    // HEADERS
    public static final String SESSION_HEADER = "Set-Cookie";
    public static final String COOKIE_HEADER = "Cookie";


    public static final String SESSION_TOKEN = "sessionToken";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String APPLICATION_FORM = "application/x-www-form-urlencoded";

    // SessionData


}
