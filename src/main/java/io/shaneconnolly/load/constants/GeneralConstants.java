package io.shaneconnolly.load.constants;


import java.util.Map;

/**
 * @author connollys
 *         21/08/15.
 */
public class GeneralConstants {

    public static void printProps(){
        System.out.println("***************************************************");
        System.out.println("******** THREADS : " + THREADS  +" **************");
        System.out.println("******** RAMP : " + RAMP_UP  +" **************");
        System.out.println("******** LOOPS : " + LOOPS  +" **************");
        System.out.println("***************************************************");
    }

    private static String getEnvironmentVariable(String envVariableName) {
        Map<String, String> env = System.getenv();
        return env.get(envVariableName);
    }


    public static final String SEC_TEST_PROXY = "http://internalproxy.shaneconolly.io";
    public static final String SEC_TEST_PROXY_PORT = "5212";

    public static final int THREADS =  2; // Integer.parseInt(getProperty("THREADS"));
    public static final int RAMP_UP = 1; // Integer.parseInt(getProperty("RAMP_UP"));
    public static final int LOOPS = 1; // Integer.parseInt(getProperty("LOOPS"));

    public static final String JMETER_HOME = getEnvironmentVariable("JMETER_HOME");
    public static final Boolean USE_CHARLES = false; // Boolean.valueOf(getProperty("USE_CHARLES"));
    public static final boolean LOG_ON = false; //Boolean.valueOf(getProperty("LOG_ON"));
    public static final Boolean VERIFY_RESPONSES = false; //Boolean.valueOf(getProperty("VERIFY_RESPONSES"));




    //Environments
    public static final String TEST_ENV = "http://shaneconnolly.io"; //getProperty("TEST_ENV");

    public static final String DEFAULT_HTTP_SCHEME = "https";
    }
