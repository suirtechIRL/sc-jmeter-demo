package io.shaneconnolly.load.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by connollys on 08/09/2015.
 */
public class SamplerConstants {

    public static final String SAMPLER_1 = "SomeGet";
    public static final String SAMPLER_2 = "SomePost";


    public static List<String> getListOfSamplers(){
        List<String> list = new ArrayList<>();
        list.add(SAMPLER_1);
        list.add(SAMPLER_2);

        return list;
    }




}
