package io.shaneconnolly.load.sharedhelpers;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import io.shaneconnolly.load.constants.GeneralConstants;
import io.shaneconnolly.load.constants.ProjectConstants;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.protocol.java.control.gui.JavaTestSamplerGui;
import org.apache.jmeter.protocol.java.sampler.JavaSampler;
import org.apache.jmeter.testelement.TestElement;

/**
 * @author connollys
 *         25/02/15.
 */
public class SetUpHelper {

    private static Gson sessionGson;
    private static Faker faker = new Faker();

    public static void initialiseLoadTests() {
        sessionGson = new Gson();
    }

    public static Gson getSessionGson() {
        if (sessionGson == null) {
            sessionGson = new Gson();
        }
        return sessionGson;
    }

    public static Faker getFaker() {
        return faker;
    }

    public static void setObjectProperty(org.apache.jmeter.threads.ThreadGroup threadGroup) {
        if (GeneralConstants.USE_CHARLES) {
            threadGroup.setProperty(HTTPSamplerBase.PROXYHOST, ProjectConstants.PROXY_HOST);
            threadGroup.setProperty(HTTPSamplerBase.PROXYPORT, String.valueOf(ProjectConstants.PROXY_PORT));
        }
    }

    public static JavaSampler createSampler(String name, String className) {
        JavaSampler sampler = new JavaSampler();
        sampler.setName(name);
        sampler.setClassname(className);
        if (GeneralConstants.USE_CHARLES) {
            sampler.setProperty(HTTPSamplerBase.PROXYHOST, ProjectConstants.PROXY_HOST);
            sampler.setProperty(HTTPSamplerBase.PROXYPORT, String.valueOf(ProjectConstants.PROXY_PORT));
        }
        sampler.setProperty(TestElement.TEST_CLASS, JavaSampler.class.getName());
        sampler.setProperty(TestElement.GUI_CLASS, JavaTestSamplerGui.class.getName());
        sampler.setEnabled(true);
        sampler.setProperty(TestElement.TEST_CLASS, className);
        sampler.setProperty(TestElement.GUI_CLASS, JavaTestSamplerGui.class.getName());

        return sampler;
    }

    public static JavaSampler createSamplerWithProxy(String name, String className) {
        LogHelper.logInfo("SETTING PROXY ");
        JavaSampler sampler = new JavaSampler();
        sampler.setName(name);
        sampler.setClassname(className);
        sampler.setProperty(HTTPSamplerBase.PROXYHOST, GeneralConstants.SEC_TEST_PROXY);
        sampler.setProperty(HTTPSamplerBase.PROXYPORT, GeneralConstants.SEC_TEST_PROXY_PORT);
        sampler.setProperty(TestElement.TEST_CLASS, JavaSampler.class.getName());
        sampler.setProperty(TestElement.GUI_CLASS, JavaTestSamplerGui.class.getName());
        sampler.setEnabled(true);
        return sampler;
    }



}
