package io.shaneconnolly.load.sharedhelpers;

import io.shaneconnolly.load.constants.ProjectConstants;
import org.apache.commons.net.ntp.TimeStamp;

import java.util.Calendar;
import java.util.Random;
import java.util.Stack;
import java.util.UUID;

/**
 * @author connollys
 *         11/06/15.
 */
public class RequestHelper {

    private static Stack signUpEmails = new Stack();
    private static Random random = new Random();

    static {
        for (int i = 0; i < 1000; i++) {
            signUpEmails.push(String.format(ProjectConstants.TESTER_EMAIL_FORMAT, i));
        }

        LogHelper.logInfo("SIZE EMAILS IS " + signUpEmails.size());
    }

    public static String getSignUpEmail() {
        return (String) signUpEmails.pop();
    }

    public static String getSignUpEmailLogin() {
        Random random = new Random();
        int Low = 1;
        int High = 999;
        int index = random.nextInt(High - Low) + Low;
        return (String) signUpEmails.get(index);
    }

    private static String getFormat(String format) {
        return String.format(format, UUID.randomUUID().toString().substring(0, 5));
    }

    public static String getUniqueEmailAddress() {
        return getFormat(ProjectConstants.LOAD_EMAIL_FORMAT);
    }

    public static String getUniqueUpdatedEmailAddress() {
        return getFormat(ProjectConstants.LOAD_UPDATED_EMAIL_FORMAT);
    }

    public static long addTimeToDate(int amountOfMonths) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, amountOfMonths);
        return new TimeStamp(cal.getTime().getTime()).ntpValue();

    }


    public static int getRandomInt(int min, int max) {
        int result = 0;
        if (max > 0) {
            result = random.nextInt(max - min) + min;
        }
        return result;
    }
}
