package io.shaneconnolly.load.sharedhelpers;

import io.shaneconnolly.load.constants.GeneralConstants;
import org.apache.jmeter.threads.JMeterContextService;

/**
 * @author connollys
 *         25/02/15.
 */
public class LogHelper {


    public static void logInfo(String message) {
        if (GeneralConstants.LOG_ON) {
            System.out.println("SC-LOAD -> " + message);
        }

    }

    public static void logSampler(int status) {
        logInfo(String.format("GOT STATUS CODE: %d for TID: %d %s", status, JMeterContextService.getContext().getThreadNum(), JMeterContextService.getContext().getCurrentSampler().getName()));
    }

    public static void logSampler() {
        logInfo(String.format("TID: %d %s", JMeterContextService.getContext().getThreadNum(), JMeterContextService.getContext().getCurrentSampler().getName()));
    }
}
