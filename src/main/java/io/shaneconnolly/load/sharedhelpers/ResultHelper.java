package io.shaneconnolly.load.sharedhelpers;

import org.apache.http.HttpResponse;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterContextService;

/**
 * @author connollys
 *         25/02/15.
 */
public class ResultHelper {

    public static SampleResult setSuccessfulResults(SampleResult sampleResult, String requestBody) {
        sampleResult.setSuccessful(true);
        sampleResult.setResponseMessage("Successfully performed Post " + "\n" + requestBody);
        sampleResult.setResponseCodeOK();
        sampleResult.setDataType(SampleResult.TEXT);
        return sampleResult;
    }

    public static SampleResult setResults(SampleResult sampleResult, String requestBody, HttpResponse response) {
        sampleResult.setSuccessful(true);
        sampleResult.setResponseMessage("Request successfully performed" + "\n" + requestBody);
        LogHelper.logInfo(String.format(">>>>>> Setting STATUS CODE: %s", String.valueOf(response.getStatusLine().getStatusCode())));
        sampleResult.setResponseCode(String.valueOf(response.getStatusLine().getStatusCode()));
        sampleResult.setDataType(SampleResult.TEXT);
        return sampleResult;
    }

    public static SampleResult setSuccessfulResults(SampleResult sampleResult) {
        sampleResult.setSuccessful(true);
        sampleResult.setResponseMessage("Successfully performed GET Request");
        sampleResult.setResponseCodeOK();
        sampleResult.setDataType(SampleResult.TEXT);
        return sampleResult;
    }


    public static SampleResult setErrorResults(SampleResult sampleResult, Exception e, String url) {
        sampleResult.setSuccessful(false);
        sampleResult.setResponseMessage("ERROR at : " + url + "\n" + e.getMessage());
        sampleResult.setDataType(SampleResult.TEXT);
        return sampleResult;
    }

    public static SampleResult setErrorResultsBadResponse(SampleResult sampleResult, String message) {
        sampleResult.setSuccessful(false);
        sampleResult.setResponseMessage(String.format("TID: %s BAD RESPONSE: %s | %s", JMeterContextService.getContext().getThreadNum(),
                message, JMeterContextService.getContext().getVariables().get("SOME_REQUEST")));
        sampleResult.setDataType(SampleResult.TEXT);
        return sampleResult;
    }

    public static void clearResultResponseData(SampleResult result) {
        if (result.isSuccessful()) {
            result.setResponseData("".getBytes());
        }
    }

}
