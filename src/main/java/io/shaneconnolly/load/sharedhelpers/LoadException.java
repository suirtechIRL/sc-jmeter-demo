package io.shaneconnolly.load.sharedhelpers;

import org.apache.http.client.ClientProtocolException;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterThread;

/**
 * @author connollys
 *         27/02/15.
 */
public class LoadException {


    public static void throwParseException(String jsonResponse) {
        try {
            JMeterThread thread = JMeterContextService.getContext().getThread();
            thread.setOnErrorStartNextLoop(true);
            LogHelper.logSampler();
            throw new ClientProtocolException("RESPONSE Unexpected response status: " + jsonResponse);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        }

    }
}
